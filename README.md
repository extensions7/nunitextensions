# NUnitExtensions



## Description
This library is a collection of quality of life extensions that make certain tests much easier and faster to write. The initial functionality makes writing tests for bad parameter handling much simpler and consistent.

## Usage

### Bad Parameter Handling
Currently, this is able to handle testing of constructors, methods (deferred also), index properties (static and instance), getter properties (also, static and instance). Setter properties are not currently as easily done, but can be handled via ParameterTests.ForMethod.

```
public class TestFixture
{
    public static object[] TestCases
    {
        get
        {
            var onlyGood = new GoodAndBadParameterValues (true);
            var bothKinds = new GoodAndBadParameterValues (0, -1, typeof(IndexOutOfBounds));

            return ParameterTests.GenerateCases (onlyGood, bothKinds);
        }
    }

    [Test, TestCaseSource (nameof (TestCases))]
    public void TestMethod (bool good, int both, Type expectedExceptionType)
    {
        // first, a static method
        var del = MethodToTest;

        ParameterTests.ForMethod (del, expectedExceptionType, good, both);

        // next, an instance method
        var obj = new CustomType ();

        del = obj.InstanceMethodToTest;

        ParameterTests.ForMethod (del, expectedExceptionType, good, both);

        // finally, a deferred method
        del = obj.ForEach;

        ParameterTests.ForDeferredMethod (del, expectedExceptionType, good, both);
    }

    [Test, TestCaseSource (nameof (TestCases))]
    public void TestConstructor (bool good, int both, Type expectedExceptionType)
    {
        var signature = new [] { good.GetType (), both.GetType () };

        var del = typeof (CustomType).GetConstructor (signature);

        ParameterTests.ForConstructor (del, expectedExceptionType, good, both);
    }
}
```

While not shown in this example, property testing is similar to method testing.

## Support
Issues should be reported in this repository's issues tab.

## Roadmap
I have some alpha code for generating test code (possibly using C# 9's code generators) based off of a class, struct, or interface. The goal is that you should be able to declare a method, property or constructor and the generator picks that up and generates a basic test and test case source property ready for you to plug in some good values, bad values and any exception type expected to be thrown.

## License
This project is licensed under Apache 2.0.

## Project status
Development on this project is currently on hold due to other projects requiring my full attention.
