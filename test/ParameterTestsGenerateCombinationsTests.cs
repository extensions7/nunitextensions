﻿/*
   Copyright 2022 ParameterTestsGenerateCombinationsTests.cs

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace NUnitExtensionsTests
{
    using System;
    using System.Collections.Generic;

    using NUnit.Framework;

    using NUnitExtensions;

    public class ParameterTestsGenerateCombinationsTests
    {
        private static int [] first = { 1, 2, 3 };

        private static int [] second = { 2, 4, 6 };

        public static IEnumerable<object []> BadArgsCases
        {
            get
            {
                Delegate del = (Func<IEnumerable<int>, IEnumerable<int>, IEnumerable<object []>>)ParameterTests.GenerateCombinations;
                var eet = typeof (ArgumentNullException);
                yield return new object [] { del, eet, first, null };
                yield return new object [] { del, eet, null, second };

                del = (Func<IList<int>, IList<int>, IEnumerable<object []>>)ParameterTests.GenerateCombinations;
                yield return new object [] { del, eet, first, null };
                yield return new object [] { del, eet, null, second };
            }
        }

        public static IEnumerable<object []> CombinationsCases
        {
            get
            {
                yield return helper (first, second);
                yield return helper (second, first);

                object [] helper (int [] a, int [] b)
                {
                    var e = new int [][]
                    {
                        new int [] { a [0], b [0] },
                        new int [] { a [0], b [1] },
                        new int [] { a [0], b [2] },

                        new int [] { a [1], b [0] },
                        new int [] { a [1], b [1] },
                        new int [] { a [1], b [2] },

                        new int [] { a [2], b [0] },
                        new int [] { a [2], b [1] },
                        new int [] { a [2], b [2] }
                    };

                    return new object [] { new List<int> (a), new List<int> (b), e };
                }
            }
        }

        [Test]
        [TestCaseSource (nameof (BadArgsCases))]
        [Order (0)]
        public void GenerateCombinationsBadArgsTest (Delegate del, Type expectedExceptionType, object a, object b)
        {
            ParameterTests.ForDeferredMethod<int> (del, expectedExceptionType, a, b);
        }

        [Test]
        [TestCaseSource (nameof (CombinationsCases))]
        [Order (1)]
        public void ShouldGenerateCombinations (List<int> a, List<int> b, int [][] expected)
        {
            helper (a, b, expected);
            helper (a as IEnumerable<int>, b as IEnumerable<int>, expected);

            void helper<T> (T ta, T tb, int [][] ex) where T : IEnumerable<int>
            {
                var actual = new List<object []> (ParameterTests.GenerateCombinations (a, b));

                for (var i = 0; i < ex.Length; i++)
                    for (var j = 0; j < ex [i].Length; j++)
                        Assert.AreEqual (ex [i] [j], (int)actual [i] [j], "Failed at [{0}][{1}]", i, j);
            }
        }
    }
}
