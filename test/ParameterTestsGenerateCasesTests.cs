﻿/*
   Copyright 2022 ParameterTestsGenerateCasesTests.cs

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace NUnitExtensionsTests
{
    using System;
    using System.Collections.Generic;

    using NUnit.Framework;

    using NUnitExtensions;

    public class ParameterTestsGenerateCasesTests
    {
        public static readonly GoodAndBadParameterValues [] UsableGoodAndBadInputs;

        public static readonly object [] UsableObjectArrayInputs;

        public static IEnumerable<object []> BadGABPVsIndexCases
        {
            get
            {
                var input = UsableGoodAndBadInputs;
                yield return new object [] { -1, input };
                yield return new object [] { input.Length + 1, input };
            }
        }

        public static IEnumerable<object []> BadObjectsIndexCases
        {
            get
            {
                var types = new Type [] { typeof (ArgumentNullException), typeof (ArgumentNullException) };
                var args = new object [] { new object (), new object () };

                yield return new object [] { -1, types, args };
                yield return new object [] { args.Length + 1, types, args };
            }
        }

        public static IEnumerable<object []> GABPVsArraysCases
        {
            get
            {
                var input = UsableGoodAndBadInputs;
                var expected = new object [] []
                    {
                        new object [] { input [0].Bad, input [1].Good, input [2].Good, input [0].ExceptionType },
                        new object [] { input [0].Good, input [1].Bad, input [2].Good, input [1].ExceptionType },
                        new object [] { input [0].Good, input [1].Good, input [2].Bad, input [2].ExceptionType }
                    };

                yield return new object [] { input, expected };
            }
        }

        public static IEnumerable<object []> GABPVsArraysWithIndexCases
        {
            get
            {
                var input = UsableGoodAndBadInputs;
                var index = 0;
                var expected = new object [] []
                    {
                        new object [] { input [0].ExceptionType, input [0].Bad, input [1].Good, input [2].Good },
                        new object [] { input [1].ExceptionType, input [0].Good, input [1].Bad, input [2].Good },
                        new object [] { input [2].ExceptionType, input [0].Good, input [1].Good, input [2].Bad }
                    };

                yield return new object [] { index, input, expected };

                index = 2;
                expected = new object [] []
                    {
                        new object [] { input [0].Bad, input [1].Good, input [0].ExceptionType, input [2].Good },
                        new object [] { input [0].Good, input [1].Bad, input [1].ExceptionType, input [2].Good },
                        new object [] { input [0].Good, input [1].Good, input [2].ExceptionType, input [2].Bad }
                    };

                yield return new object [] { index, input, expected };
            }
        }

        public static IEnumerable<object []> GenerateForBadArgsCases
        {
            get
            {
                var del = (Action<Type, object, object>)TypeAtStart;
                var exType = typeof (ArgumentNullException);
                var testMethod = new GoodAndBadParameterValues (del, null, exType);
                var index = new GoodAndBadParameterValues (1, -1, typeof (ArgumentException));
                var array = new GoodAndBadParameterValues []
                    {
                        new GoodAndBadParameterValues (new object (), null, exType),
                        new GoodAndBadParameterValues (new object (), null, exType)
                    };

                var args = new GoodAndBadParameterValues (array, null, exType);

                return ParameterTests.GenerateCases (testMethod, index, args);
            }
        }

        public static IEnumerable<object []> GenerateForDelegateCases
        {
            get
            {
                object a = new object (), b = new object ();
                Type nre = typeof (NullReferenceException), ane = typeof (ArgumentNullException);
                var args = new GoodAndBadParameterValues []
                    {
                        new GoodAndBadParameterValues (a, null, nre),
                        new GoodAndBadParameterValues (b, null, ane)
                    };

                Delegate del = (Action<object, object, Type>)TypeAtEnd;
                var expected = new object [] []
                    {
                        new object [] { null, b, nre },
                        new object [] { a, null, ane }
                    };

                yield return new object [] { del, 2, args, expected };

                del = (Action<Type, object, object>)TypeAtStart;
                expected = new object [] []
                    {
                        new object [] { nre, null, b },
                        new object [] { ane, a, null }
                    };

                yield return new object [] { del, 0, args, expected };

                // This might give me some trouble because args' exceptions are different. However Exception is the base type for all exceptions.
                del = (GenAction<Exception, object, object>)TypeAsTypeParam<Exception>;
                expected = new object [] []
                    {
                        new object [] { null, b, nre },
                        new object [] { a, null, ane }
                    };

                yield return new object [] { del, -1, args, expected };
            }
        }

        public static IEnumerable<object []> ObjectArraysCases
        {
            get
            {
                var types = new Type [] { typeof (ArgumentException), typeof (ArgumentNullException), typeof (Exception) };
                var args = UsableObjectArrayInputs;
                var expected = new object [] []
                    {
                        new object [] { null, args [1], args [2], types [0] },
                        new object [] { args [0], null, args [2], types [1] },
                        new object [] { args [0], args [1], null, types [2] }
                    };

                yield return new object [] { types, args, expected };
            }
        }

        public static IEnumerable<object []> ObjectArraysWithIndexCases
        {
            get
            {
                var types = new Type [] { typeof (ArgumentException), typeof (ArgumentNullException), typeof (Exception) };
                var args = UsableObjectArrayInputs;
                var index = 0;
                var expected = new object [][]
                    {
                        new object [] { types [0], null, args [1], args [2] },
                        new object [] { types [1], args [0], null, args [2] },
                        new object [] { types [2], args [0], args [1], null }
                    };

                yield return new object [] { index, types, args, expected };

                index = 2;
                expected = new object [][]
                    {
                        new object [] { null, args [1], types [0], args [2] },
                        new object [] { args [0], null, types [1], args [2] },
                        new object [] { args [0], args [1], types [2], null }
                    };

                yield return new object [] { index, types, args, expected };
            }
        }

        public static IEnumerable<object []> ObjectArraysWithOneTypeCases
        {
            get
            {
                var type = typeof (ArgumentException);
                var args = UsableObjectArrayInputs;
                var expected = new object [][]
                    {
                        new object [] { null, args [1], args [2], type },
                        new object [] { args [0], null, args [2], type },
                        new object [] { args [0], args [1], null, type }
                    };

                yield return new object [] { type, args, expected };
            }
        }

        static ParameterTestsGenerateCasesTests ()
        {
            var objs = new object [] { new object (), new object (), new object () };

            var type = typeof (ArgumentException);
            var a = new GoodAndBadParameterValues (1, -1, type);
            var b = new GoodAndBadParameterValues (2, -2, type);
            var c = new GoodAndBadParameterValues (3, -3, type);
            var gabs = new GoodAndBadParameterValues [] { a, b, c };

            UsableGoodAndBadInputs = gabs;
            UsableObjectArrayInputs = objs;
        }

        private static void TypeAtEnd (object a, object b, Type type) { }

        private static void TypeAtStart (Type type, object a, object b) { }

        private static void TypeAsTypeParam<TException> (object a, object b) where TException : Exception { }

        private void AssertObjectArraysAreEqual (IEnumerable<object []> actuals, object [][] expected)
        {
            var index = 0;
            foreach (var actual in actuals)
            {
                Assert.AreEqual (expected [index].Length, actual.Length, "Array lengths are not equal.");
                for (var i = 0; i < actual.Length; i++)
                {
                    Assert.AreEqual (expected [index] [i], actual [i], "index={0}; i={1}", index, i);
                }

                index++;
            }
        }

        // This is for testing GenerateTestCases
        // There are 2 overloads that we should be primarily concerned with.
        // They both accept an int for the exception type to be inserted.
        // The variants are where the entire collection is a GoodAndBadParameterValues [] and an object [].
        // There should be a test where a good array is passed in and the int parameter is 0 or beyond the length of the array.
        // All overloads should have a test where an input parameter is null or of the wrong length (and in the object [] case, have a value type in it)
        // For the GoodAndBadParameterValues array overload, there should be ...
        //      one case with all values having only good values
        //      one case with all good and bad values
        //      one case with a mixed set.
        //
        // For the object array overload, there should be ...
        //      one case with all GoodAndBadParameterValues values
        //      one case with all nullable objects
        //      one case with a mixed set

        [Test]
        [Order (1)]
        public void ShouldThrowArgNullExceptionInGABPVArrayMethod ()
        {
            Func<int, GoodAndBadParameterValues [], IEnumerable<object []>> func = ParameterTests.GenerateCases;
            GoodAndBadParameterValues [] gabs = null;
            var args = new object [] { 0, gabs };

            ParameterTests.ForDeferredMethod<object [], ArgumentNullException> (func, args);
        }

        [Test]
        [TestCaseSource (nameof (BadGABPVsIndexCases))]
        [Order (2)]
        public void ShouldThrowArgExceptionForBadIndexInGABPVArrayMethod (int badIndex, GoodAndBadParameterValues [] input)
        {
            Func<int, GoodAndBadParameterValues [], IEnumerable<object []>> func = ParameterTests.GenerateCases;

            ParameterTests.ForDeferredMethod<object [], ArgumentException> (func, badIndex, input);
        }

        [Test]
        [TestCaseSource (nameof (GABPVsArraysWithIndexCases))]
        [Order (3)]
        public void ShouldGenerateObjectArraysUsingGABPVsArrayAndIndex (int index, GoodAndBadParameterValues [] input, object [][] expected)
        {
            var results = ParameterTests.GenerateCases (index, input);

            AssertObjectArraysAreEqual (results, expected);
        }

        [Test]
        [TestCaseSource (nameof (GABPVsArraysCases))]
        [Order (4)]
        public void ShouldGenerateObjectArraysUsingGABPVsArray (GoodAndBadParameterValues [] input, object [][] expected)
        {
            var results = ParameterTests.GenerateCases (input);

            AssertObjectArraysAreEqual (results, expected);
        }

        [Test]
        [Order (5)]
        public void ShouldThrowArgNullExceptionInObjectArrayMethod ()
        {
            var types = new Type [] { typeof (ArgumentNullException), typeof (ArgumentNullException) };
            var args = new object [] { new object (), new object () };
            Func<int, Type [], object [], IEnumerable<object []>> func = ParameterTests.GenerateCases;

            ParameterTests.ForDeferredMethod<object [], ArgumentNullException> (func, 0, types, null);
            ParameterTests.ForDeferredMethod<object [], ArgumentNullException> (func, 0, null, args);
        }

        [Test]
        [TestCaseSource (nameof (BadObjectsIndexCases))]
        [Order (6)]
        public void ShouldThrowArgExceptionForBadIndexInObjectArrayMethod (int badIndex, Type [] types, object [] args)
        {
            Func<int, Type [], object [], IEnumerable<object []>> func = ParameterTests.GenerateCases;

            ParameterTests.ForDeferredMethod<object [], ArgumentException> (func, badIndex, types, args);
        }

        [Test]
        [TestCaseSource (nameof (ObjectArraysWithIndexCases))]
        [Order (7)]
        public void ShouldGenerateObjectArrayUsingObjectArrayAndIndex (int index, Type [] types, object [] args, object [][] expected)
        {
            var results = ParameterTests.GenerateCases (index, types, args);

            AssertObjectArraysAreEqual (results, expected);
        }

        [Test]
        [TestCaseSource (nameof (ObjectArraysCases))]
        [Order (8)]
        public void ShouldGenerateObjectArrayUsingObjectArray (Type [] types, object [] args, object [][] expected)
        {
            var results = ParameterTests.GenerateCases (types, args);

            AssertObjectArraysAreEqual (results, expected);
        }

        [Test]
        [TestCaseSource (nameof (ObjectArraysWithOneTypeCases))]
        [Order (9)]
        public void ShouldGenerateObjectArrayUsingObjectArrayAndOneExceptionType (Type type, object [] args, object [][] expected)
        {
            var results = ParameterTests.GenerateCases (type, args);

            AssertObjectArraysAreEqual (results, expected);
        }

        [Test]
        [TestCaseSource (nameof (GenerateForBadArgsCases))]
        [Order (10)]
        public void GenerateTestCasesForShouldThrowException (Delegate testMethod, int expectedExceptionIndex, GoodAndBadParameterValues [] args, Type expectedException)
        {
            var withIndex = (Func<Delegate, int, GoodAndBadParameterValues [], IEnumerable<object []>>)ParameterTests.GenerateCasesFor;

            ParameterTests.ForDeferredMethod<object []> (withIndex, expectedException, testMethod, expectedExceptionIndex, args);

            var withoutIndex = (Func<Delegate, GoodAndBadParameterValues [], IEnumerable<object []>>)ParameterTests.GenerateCasesFor;

            ParameterTests.ForDeferredMethod<object []> (withoutIndex, expectedException, testMethod, args);
        }

        [Test]
        [TestCaseSource (nameof (GenerateForDelegateCases))]
        [Order (11)]
        public void ShouldGenerateObjectArrayForDelegate (Delegate testMethod, int expectedExceptionIndex, GoodAndBadParameterValues [] args, object [][] expected)
        {
            var result = ParameterTests.GenerateCasesFor (testMethod, expectedExceptionIndex, args);

            var actual = System.Linq.Enumerable.ToArray (result);

            var isGeneric = testMethod.Method.IsGenericMethod;
            for (var o = 0; o < expected.Length; o++)
            {
                var innerMax = isGeneric ? expected.Length - 1 : expected.Length;
                for (var i = 0; i < innerMax; i++)
                {
                    Assert.AreEqual (expected [o][i], actual [o][i]);
                }
            }
        }
    }
}
