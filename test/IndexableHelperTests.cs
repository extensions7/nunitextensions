﻿/*
   Copyright 2022 IndexableHelperTests.cs

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace NUnitExtensionsTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    using NUnit.Framework;

    using NUnitExtensions;

    public class IndexableHelperTests
    {
        private static int [] values = { 1, 2, 3 };

        public static IEnumerable<ConstructorInfo> ConstructorInfos
        {
            get
            {
                var flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.CreateInstance;
                return typeof (IndexableHelper<object>).GetConstructors (flags);
            }
        }

        public static IEnumerable<object []> GetCountCases
        {
            get
            {
                var inputs = new List<int> []
                    {
                        new List<int> (),
                        new List<int> { 1, 2 },
                        new List<int> (values)
                    };

                var counts = new int [] { 0, 2, 3 };

                for (var i = 0; i < counts.Length; i++)
                {
                    yield return new object [] { new IndexableHelper<int> (inputs [i]), counts [i] };
                    yield return new object [] { new IndexableHelper<int> (inputs [i] as IReadOnlyList<int>), counts [i] };
                }
            }
        }

        public static IEnumerable<object []> GetterPropertyCases
        {
            get
            {
                yield return new object [] { new IndexableHelper<int> (values) };
                yield return new object [] { new IndexableHelper<int> (values as IReadOnlyList<int>) };
            }
        }

        [Test]
        [TestCaseSource (nameof (ConstructorInfos))]
        [Order (0)]
        public void ConstructorShouldThrowWithBadParams (ConstructorInfo ctor)
        {
            ParameterTests.ForConstructor<ArgumentNullException> (ctor, ParameterTests.OneNullArg);
        }

        [Test]
        [TestCaseSource (nameof (GetCountCases))]
        [Order (1)]
        public void ShouldGetCount (IndexableHelper<int> testee, int expected)
        {
            Assert.AreEqual (expected, testee.Count, "Count failed.");
        }

        [Test]
        [TestCaseSource (nameof (GetterPropertyCases))]
        [Order (2)]
        public void ShouldGetValue (IndexableHelper<int> testee)
        {
            for (var i = 0; i < values.Length; i++)
                Assert.AreEqual (values [i], testee [i], "Index getter failed.");
        }
    }
}
