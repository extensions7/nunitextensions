﻿/*
   Copyright 2022 DebugStringGeneratorTests.cs

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace NUnitExtensionsTests
{
    using System.Collections.Generic;

    using NUnit.Framework;

    using NUnitExtensions;

    public class DebugStringGeneratorTests
    {
        public static IEnumerable<object []> TestCases
        {
            get
            {
                var format = DebugStringGenerator.Format;

                var a = new int [] { 0, 1 };
                var b = new int [] { 2, 3 };
                var expected = string.Format (format, "0, 1", "2, 3");

                yield return new object [] { a, b, expected };

                a = new int [] { 10, 11 };
                b = new int [] { 12, 13 };
                expected = string.Format (format, "10, 11", "12, 13");

                yield return new object [] { a, b, expected };
            }
        }

        [Test]
        [TestCaseSource (nameof (TestCases))]
        public static void ShouldGenerateDebugString (int [] inputA, int [] inputB, string expected)
        {
            var actual = DebugStringGenerator.Generate (inputA, inputB);

            Assert.AreEqual (expected, actual);
        }
    }
}
