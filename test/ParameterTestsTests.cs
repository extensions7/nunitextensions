﻿/*
   Copyright 2022 ParameterTestsTests.cs

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace NUnitExtensionsTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using NUnit.Framework;

    using NUnitExtensions;

    public static class ParameterTestsTests
    {
        private static void ThrowIfNull (this object obj, string name)
        {
            if (ReferenceEquals (obj, null))
                throw new ArgumentNullException (name);
        }

        // Simple object for testing the above tests

        private class Foo
        {
            public static object StaticThrower { get { throw new NullReferenceException (); } }

            public object? NullMaker { get { return null; } }

            public object Thrower { get { throw new NullReferenceException (); } }

            public object InvalidOperationThrower {
                get {
                    throw new InvalidOperationException ();
                }
                set {
                    throw new InvalidOperationException ();
                }
            }

            public object this [int index] {
                get {
                    throw new NullReferenceException ();
                }
            }

            public Foo (object a)
            {
                a.ThrowIfNull ("a");
            }

            public void Bar (object a)
            {
                a.ThrowIfNull ("a");
            }

            public IEnumerable<object> MakeObjects (bool doWork)
            {
                if (doWork) {
                    for (var i = 0; i < 5; i++) {
                        if (i == 0)
                            throw new Exception ();

                        yield return new object ();
                    }
                }

                throw new Exception ();
            }
        }

        [Test]
        [Order (1)]
        public static void ShouldPassConstructorParameterTest ()
        {
            var type = typeof (object);
            var types = new Type [] { type };
            var ctor = typeof (Foo).GetConstructor (types);

            ParameterTests.ForConstructor (ctor, typeof (ArgumentNullException), new object [] { null });
        }

        [Test]
        [Order (2)]
        public static void ShouldPassMethodParameterTest ()
        {
            var type = typeof (object);
            var types = new Type [] { type };
            var testee = new Foo (new object ());
            Action<object> del = testee.Bar;

            ParameterTests.ForMethod<ArgumentNullException> (del, ParameterTests.OneNullArg);
        }

        private const string description = "Normally this kind of test should never be written. However, this is a test testing another test, so all outcomes need to be covered.";
        private const string reason = "This is an abnormal test because it is supposed to fail. Run this test only when necessary.";

        [Test]
        [Order (3)]
        [Ignore (reason)]
        [Description (description)]
        public static void ShouldFailConstructorParameterTest ()
        {
            var input = new object ();
            var types = new Type [] { input.GetType () };
            var ctor = typeof (Foo).GetConstructor (types);

            ParameterTests.ForConstructor<Exception> (ctor, new object [] { input });
        }

        [Test]
        [Order (4)]
        [Ignore (reason)]
        [Description (description)]
        public static void ShouldFailMethodParameterTest ()
        {
            var input = new object ();
            var types = new Type [] { input.GetType () };
            var testee = new Foo (input);
            Action<object> del = testee.Bar;

            ParameterTests.ForMethod<Exception> (del, new object [] { input });
        }

        [Test]
        [TestCase (false)]
        [TestCase (true)]
        [Order (5)]
        public static void ShouldWrapDeferredMethodCorrectly (bool input)
        {
            var foo = new Foo (new object ());
            Func<bool, IEnumerable<object>> func = foo.MakeObjects;

            ParameterTests.ForDeferredMethod<object, Exception> (func, input);
        }

        [Test]
        [TestCase (typeof (Func<object>), false)]
        [TestCase (typeof (Func<int, object>), true)]
        [Order (6)]
        public static void ShouldReturnDelegatePropertyType (Type expected, bool useIndexer)
        {
            var fooType = typeof (Foo);
            var methodInfo = useIndexer ?
                fooType.GetProperties ().First (i => i.GetIndexParameters ().Length > 0) :
                fooType.GetProperty (nameof (Foo.StaticThrower));
            var actual = ParameterTests.GetPropertyDelegateType (methodInfo.GetMethod);

            Assert.AreEqual (expected, actual);
        }

        [Test]
        [Order (7)]
        public static void StaticPropertyShouldThrowException ()
        {
            var prop = typeof (Foo).GetProperty (nameof (Foo.StaticThrower));

            ParameterTests.ForStaticProperty<NullReferenceException> (prop);
        }

        [Test]
        [Order (8)]
        public static void InstancePropertyShouldThrowException ()
        {
            var foo = new Foo (new object ());
            var prop = foo.GetType ().GetProperty (nameof (foo.Thrower));

            ParameterTests.ForInstanceProperty<NullReferenceException> (prop, foo);
        }
    }
}