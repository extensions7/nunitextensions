﻿/*
   Copyright 2022 GoodAndBadParameterValues.cs

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace NUnitExtensions
{
    public struct GoodAndBadParameterValues
    {
        public object Good;

        public object? Bad;

        public System.Type ParameterType;

        public System.Type? ExceptionType;

        public bool IsUsable { get => !ReferenceEquals (this.ExceptionType, null); }

        private GoodAndBadParameterValues(object good, object? bad, System.Type type, System.Type? exceptionType)
        {
            this.Good = good;
            this.Bad = bad;
            this.ParameterType = type;
            this.ExceptionType = exceptionType;
        }

        public GoodAndBadParameterValues (object good, object bad, System.Type exceptionType) :
            this (good, bad, good.GetType (), exceptionType)
        { }

        public GoodAndBadParameterValues (object good) :
            this (good, null, good.GetType (), null)
        { }

        private static string ValueToString (object? value)
        {
            return value.IsNull() ? "null" : value.ToString();
        }

        public override string ToString()
        {
            var good = ValueToString(this.Good);
            var bad = ValueToString(this.Bad);
            var pt = ValueToString(this.ParameterType);
            var et = ValueToString(this.ExceptionType);
            return $"{nameof(GoodAndBadParameterValues)}:{nameof(Good)}={good} {nameof(Bad)}={bad} {nameof(ParameterType)}={pt} {nameof(ExceptionType)}={et}";
        }
    }
}
