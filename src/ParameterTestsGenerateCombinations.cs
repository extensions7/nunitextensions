﻿/*
   Copyright 2022 ParameterTestsGenerateCombinations.cs

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace NUnitExtensions
{
    using System;
    using System.Collections.Generic;

    public static partial class ParameterTests
    {
        private const string TooShortFormat = "\"{0}\" must have a length greater than 0.";

        private static void CheckGenerateCombinationsParameters (object a, object b, string nameofA, string nameofB)
        {
            if (a.IsNull ())
                throw new ArgumentNullException (nameofA);

            if (b.IsNull ())
                throw new ArgumentNullException (nameofB);
        }

        private static void CheckGenerateCombinationsCollectionCount<T> (IndexableHelper<T> a, string nameofA)
        {
            if (a.Count == 0)
                throw new ArgumentException (string.Format (TooShortFormat, nameofA), nameofA);
        }

        private static IEnumerable<object []> GenerateCombinationsFromIndexables<TStruct, TAny> (IndexableHelper<TStruct> a, IndexableHelper<TAny> b)
        {
            CheckGenerateCombinationsParameters (a, b, nameof (a), nameof (b));

            CheckGenerateCombinationsCollectionCount (a, nameof (a));
            CheckGenerateCombinationsCollectionCount (b, nameof (b));

            for (var i = 0; i < a.Count; i++)
                for (var j = 0; j < b.Count; j++)
                    yield return new object [] { a [i], b [j] };
        }

        private static IEnumerable<object []> GenerateCombinationsWithIndexable<TStruct, TAny> (IEnumerable<TStruct> a, IndexableHelper<TAny> b)
            where TStruct : struct
        {
            CheckGenerateCombinationsParameters (a, b, nameof (a), nameof (b));

            CheckGenerateCombinationsCollectionCount (b, nameof (b));

            var aIsEmpty = true;
            foreach (var item in a)
            {
                aIsEmpty = false;
                for (var i = 0; i < b.Count; i++)
                {
                    yield return new object [] { item, b [i] };
                }
            }

            if (aIsEmpty)
                throw new ArgumentException (string.Format (TooShortFormat, nameof (a)), nameof (a));
        }

        /// <summary>
        /// Generates all combinations from the provided enumerable collections. Note that <paramref name="b"/> gets converted to an array. It is suggested that the collection that enumerates slowere be placed there.
        /// </summary>
        /// <returns>All possible combinations.</returns>
        /// <param name="a">A collection to enumerate that is one set of objects to combine.</param>
        /// <param name="b">A collection, that will be converted into an array for performance reasons, that provides the other set of objects to combine.</param>
        /// <typeparam name="TStruct">Any value type. Reference types are not accepted due to how iterators work. If wanting to use a reference type, consider converting <paramref name="a"/> to an array.</typeparam>
        /// <typeparam name="TAny">Any type of object due to the collection containing it will be converted to an array.</typeparam>
        public static IEnumerable<object []> GenerateCombinations<TStruct, TAny> (IEnumerable<TStruct> a, IEnumerable<TAny> b)
            where TStruct : struct
        {
            IndexableHelper<TAny> indexable;
            if (b is IReadOnlyList<TAny>)
                indexable = new IndexableHelper<TAny> (b as IReadOnlyList<TAny>);
            else if (b is IList<TAny>)
                indexable = new IndexableHelper<TAny> (b as IList<TAny>);
            else
                indexable = new IndexableHelper<TAny> (System.Linq.Enumerable.ToArray (b));

            return GenerateCombinationsWithIndexable (a, indexable);
        }

        /// <summary>
        /// Generates all combinations from the provided collections.
        /// </summary>
        /// <returns>All possible combinations.</returns>
        /// <param name="a">An indexable collection that is one set of objects to combine.</param>
        /// <param name="b">A collection to enumerate that is the other set of objects to combine.</param>
        /// <typeparam name="TStruct">Any value type. Reference types are not accepted due to how iterators work. If wanting to use a reference type, consider converting <paramref name="a"/> to an array.</typeparam>
        /// <typeparam name="TAny">Any type of object.</typeparam>
        public static IEnumerable<object []> GenerateCombinations<TStruct, TAny> (IList<TAny> a, IEnumerable<TStruct> b)
            where TStruct : struct => GenerateCombinationsWithIndexable (b, new IndexableHelper<TAny> (a));

        /// <summary>
        /// Generates all combinations from the provided collections.
        /// </summary>
        /// <returns>All possible combinations.</returns>
        /// <param name="a">A collection to enumerate that is one set of objects to combine.</param>
        /// <param name="b">An indexable collection that is the other set of objects to combine.</param>
        /// <typeparam name="TStruct">Any value type. Reference types are not accepted due to how iterators work. If wanting to use a reference type, consider converting <paramref name="a"/> to an array.</typeparam>
        /// <typeparam name="TAny">Any type of object.</typeparam>
        public static IEnumerable<object []> GenerateCombinations<TStruct, TAny> (IEnumerable<TStruct> a, IList<TAny> b)
            where TStruct : struct => GenerateCombinationsWithIndexable (a, new IndexableHelper<TAny> (b));

        /// <summary>
        /// Generates all combinations from the provided collections.
        /// </summary>
        /// <returns>All possible combinations.</returns>
        /// <param name="a">An indexable collection that is one set of objects to combine.</param>
        /// <param name="b">A collection to enumerate that is the other set of objects to combine.</param>
        /// <typeparam name="TStruct">Any value type. Reference types are not accepted due to how iterators work. If wanting to use a reference type, consider converting <paramref name="a"/> to an array.</typeparam>
        /// <typeparam name="TAny">Any type of object.</typeparam>
        public static IEnumerable<object []> GenerateCombinations<TStruct, TAny> (IReadOnlyList<TAny> a, IEnumerable<TStruct> b)
            where TStruct : struct => GenerateCombinationsWithIndexable (b, new IndexableHelper<TAny> (a));

        /// <summary>
        /// Generates all combinations from the provided collections.
        /// </summary>
        /// <returns>All possible combinations.</returns>
        /// <param name="a">A collection to enumerate that is one set of objects to combine.</param>
        /// <param name="b">An indexable collection that is the other set of objects to combine.</param>
        /// <typeparam name="TStruct">Any value type. Reference types are not accepted due to how iterators work. If wanting to use a reference type, consider converting <paramref name="a"/> to an array.</typeparam>
        /// <typeparam name="TAny">Any type of object.</typeparam>
        public static IEnumerable<object []> GenerateCombinations<TStruct, TAny> (IEnumerable<TStruct> a, IReadOnlyList<TAny> b)
            where TStruct : struct => GenerateCombinationsWithIndexable (a, new IndexableHelper<TAny> (b));

        /// <summary>
        /// Generates all combinations from the provided indexable collections.
        /// </summary>
        /// <returns>All possible combinations.</returns>
        /// <param name="a">An indexable collection that is one set of objects to combine.</param>
        /// <param name="b">An indexable collection that is the other set of objects to combine.</param>
        /// <typeparam name="TAny1">Any type of object.</typeparam>
        /// <typeparam name="TAny2">Any type of object.</typeparam>
        public static IEnumerable<object []> GenerateCombinations<TAny1, TAny2> (IReadOnlyList<TAny1> a, IReadOnlyList<TAny2> b)
            => GenerateCombinationsFromIndexables (new IndexableHelper<TAny1> (a), new IndexableHelper<TAny2> (b));

        /// <summary>
        /// Generates all combinations from the provided indexable collections.
        /// </summary>
        /// <returns>All possible combinations.</returns>
        /// <param name="a">An indexable collection that is one set of objects to combine.</param>
        /// <param name="b">An indexable collection that is the other set of objects to combine.</param>
        /// <typeparam name="TAny1">Any type of object.</typeparam>
        /// <typeparam name="TAny2">Any type of object.</typeparam>
        public static IEnumerable<object []> GenerateCombinations<TAny1, TAny2> (IList<TAny1> a, IList<TAny2> b)
            => GenerateCombinationsFromIndexables (new IndexableHelper<TAny1> (a), new IndexableHelper<TAny2> (b));

        /// <summary>
        /// Generates all combinations from the provided arrays.
        /// </summary>
        /// <returns>All possible combinations.</returns>
        /// <param name="a">An array that is one set of objects to combine.</param>
        /// <param name="b">An array that is the other set of objects to combine.</param>
        /// <typeparam name="TAny1">Any type of object.</typeparam>
        /// <typeparam name="TAny2">Any type of object.</typeparam>
        public static IEnumerable<object []> GenerateCombinations<TAny1, TAny2> (TAny1 [] a, TAny2 [] b)
            => GenerateCombinationsFromIndexables (new IndexableHelper<TAny1> (a), new IndexableHelper<TAny2> (b));

        /// <summary>
        /// Generates all combinations from the provided lists.
        /// </summary>
        /// <returns>All possible combinations.</returns>
        /// <param name="a">A <see cref="List{TAny1}"/> that is one set of objects to combine.</param>
        /// <param name="b">A <see cref="List{TAny2}"/>that is the other set of objects to combine.</param>
        /// <typeparam name="TAny1">Any type of object.</typeparam>
        /// <typeparam name="TAny2">Any type of object.</typeparam>
        public static IEnumerable<object []> GenerateCombinations<TAny1, TAny2> (List<TAny1> a, List<TAny2> b)
            => GenerateCombinationsFromIndexables (new IndexableHelper<TAny1> (a), new IndexableHelper<TAny2> (b));
    }
}
