﻿/*
   Copyright 2022 ParameterTests.cs

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace NUnitExtensions
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;

    using NUnit.Framework;

    public static partial class ParameterTests
    {
        

        private const string property_error_message_part1 = "This is for ",
            property_error_message_part2 = " properties. For ",
            property_error_message_part3 = " properties, call ",
            _inst = "instance", _stat = "static",
            static_only_property_error_message = property_error_message_part1 + _stat + property_error_message_part2 + _inst + property_error_message_part3,
            instance_only_property_error_message = property_error_message_part1 + _inst + property_error_message_part2 + _stat + property_error_message_part3;

        private static void AssertIfInstanceOrStatic (MethodInfo method, bool isStatic)
        {
            AssertIfInstanceOrStatic (method, method.Name, isStatic);
        }

        private static void AssertIfInstanceOrStatic (MethodInfo method, string methodName, bool isStatic)
        {
            if (method.IsStatic != isStatic)
                Assert.Fail ("Method \"{0}\" should be {1} but is not.", methodName, isStatic ? _stat : _inst);
        }

        private static bool DelegateForIEnumerable (Delegate del, object [] args)
        {
            var output = del.DynamicInvoke (args);
            var able = output as IEnumerable;
            var ator = able.GetEnumerator ();
            var result = ator.MoveNext ();
            var current = ator.Current;
            return result;
        }

        private static bool DelegateForIEnumerator (Delegate del, object [] args)
        {
            var output = del.DynamicInvoke (args);
            var ator = output as IEnumerator;
            var result = ator.MoveNext ();
            var current = ator.Current;
            return result;
        }

        private static void IndexGetterPropertyTestHelper (Type targetType, object target, string propName, Type expectedExceptionType, string errorMessageEnding, int indexCount)
        {
            var prop = targetType.GetProperties ().First (i => i.GetIndexParameters ().Length == indexCount);

            GetterPropertyTestHelper (prop, target, propName + " index property", expectedExceptionType, errorMessageEnding, indexCount);
        }

        private static void GetterPropertyTestHelper (PropertyInfo prop, object target, string propName, Type expectedExceptionType, string errorMessageEnding, int indexCount)
        {
            var isStatic = target.IsNull ();

            var method = prop.GetMethod;
            if ((method.IsStatic && !isStatic) || (!method.IsStatic && isStatic))
                Assert.Fail ("Method \"{0}\" is {1} but {2} is not null.", propName, method.IsStatic ? _stat : _inst, nameof (target));

            var type = GetPropertyDelegateType (method);
            var del = isStatic ? method.CreateDelegate (type) : method.CreateDelegate (type, target);

            var args = indexCount > 0 ? new object [] { new int [indexCount] } : new object [] { };

            MoreAsserts.ExceptionThrown (del.DynamicInvoke, "Property: " + propName, expectedExceptionType, errorMessageEnding, args);
        }

        /// <summary>
        /// Gets the type of the property from the provided <see cref="MethodInfo"/> representing the property's getter.
        /// </summary>
        /// <returns>The property delegate type.</returns>
        /// <param name="method">The property getter.</param>
        internal static Type GetPropertyDelegateType (MethodInfo method)
        {
            var parms = method.GetParameters ();
            var types = parms.Select (p => p.ParameterType);
            var sig = new List<Type> (types);
            sig.Add (method.ReturnType);

            return Expression.GetFuncType (sig.ToArray ());
        }


        /// <summary>
        /// Tests that the provided constructor throws the appropriate exception given the provided arguments.
        /// </summary>
        /// <param name="ctor">The <see cref="ConstructorInfo"/> representing the constructor to test.</param>
        /// <param name="args">The arguments to pass in to <paramref name="ctor"/>.</param>
        /// <typeparam name="TException">The expected exception type parameter.</typeparam>
        public static void ForConstructor<TException> (ConstructorInfo ctor, params object[] args) where TException : Exception
        {
            ForConstructor<TException> (ctor, string.Empty, args);
        }

        /// <summary>
        /// Tests that the provided constructor throws the appropriate exception given the provided arguments.
        /// </summary>
        /// <param name="ctor">The <see cref="ConstructorInfo"/> representing the constructor to test.</param>
        /// <param name="args">The arguments to pass in to <paramref name="ctor"/>.</param>
        /// <param name="errorMessageEnding">A message to include after any test failures.</param>
        /// <typeparam name="TException">The expected exception type parameter.</typeparam>
        public static void ForConstructor<TException> (ConstructorInfo ctor, string errorMessageEnding, params object [] args) where TException : Exception
        {
            ForConstructor (ctor, typeof (TException), errorMessageEnding, args);
        }

        /// <summary>
        /// Tests that the provided constructor throws the appropriate exception given the provided arguments.
        /// </summary>
        /// <param name="ctor">The <see cref="ConstructorInfo"/> representing the constructor to test.</param>
        /// <param name="expectedExceptionType">The <see cref="Type"/> of the exception expected to the be thrown.</param>
        /// <param name="args">The arguments to pass in to <paramref name="ctor"/>.</param>
        public static void ForConstructor (ConstructorInfo ctor, Type expectedExceptionType, params object[] args)
        {
            ForConstructor (ctor, expectedExceptionType, string.Empty, args);
        }

        /// <summary>
        /// Tests that the provided constructor throws the appropriate exception given the provided arguments.
        /// </summary>
        /// <param name="ctor">The <see cref="ConstructorInfo"/> representing the constructor to test.</param>
        /// <param name="expectedExceptionType">The <see cref="Type"/> of the exception expected to the be thrown.</param>
        /// <param name="errorMessageEnding">A message to include after any test failures.</param>
        /// <param name="args">The arguments to pass in to <paramref name="ctor"/>.</param>
        public static void ForConstructor (ConstructorInfo ctor, Type expectedExceptionType, string errorMessageEnding, params object [] args)
        {
            MoreAsserts.ExceptionThrown (ctor.Invoke, "Constructor", expectedExceptionType, errorMessageEnding, args);
        }


        /// <summary>
        /// Tests that the provided deferred execution method throws the appropriate exception given the provided arguments.
        /// </summary>
        /// <param name="del">A delegate referencing a method, constructor or property.</param>
        /// <param name="args">The arguments to pass in to <paramref name="del"/>.</param>
        /// <typeparam name="TReturn">The type parameter assigned to the <see cref="IEnumerable{T}"/> or <see cref="IEnumerator{T}"/>.</typeparam>
        /// <typeparam name="TException">The expected exception type parameter.</typeparam>
        public static void ForDeferredMethod<TReturn, TException> (Delegate del, params object [] args) where TException : Exception
        {
            ForDeferredMethod<TReturn, TException> (del, string.Empty, args);
        }

        /// <summary>
        /// Tests that the provided deferred execution method throws the appropriate exception given the provided arguments.
        /// </summary>
        /// <param name="del">A delegate referencing a method, constructor or property.</param>
        /// <param name="errorMessageEnding">A message to include after any test failures.</param>
        /// <param name="args">The arguments to pass in to <paramref name="del"/>.</param>
        /// <typeparam name="TReturn">The type parameter assigned to the <see cref="IEnumerable{T}"/> or <see cref="IEnumerator{T}"/>.</typeparam>
        /// <typeparam name="TException">The expected exception type parameter.</typeparam>
        public static void ForDeferredMethod<TReturn, TException> (Delegate del, string errorMessageEnding, params object [] args) where TException : Exception
        {
            ForDeferredMethod<TReturn> (del, typeof (TException), errorMessageEnding, args);
        }

        /// <summary>
        /// Tests that the provided deferred execution method throws the appropriate exception given the provided arguments.
        /// </summary>
        /// <param name="del">A delegate referencing a method, constructor or property.</param>
        /// <param name="expectedExceptionType">The <see cref="Type"/> of the exception expected to the be thrown.</param>
        /// <param name="args">The arguments to pass in to <paramref name="del"/>.</param>
        /// <typeparam name="TReturn">The type parameter assigned to the <see cref="IEnumerable{T}"/> or <see cref="IEnumerator{T}"/>.</typeparam>
        public static void ForDeferredMethod<TReturn> (Delegate del, Type expectedExceptionType, params object [] args)
        {
            ForDeferredMethod<TReturn> (del, expectedExceptionType, string.Empty, args);
        }

        /// <summary>
        /// Tests that the provided deferred execution method throws the appropriate exception given the provided arguments.
        /// </summary>
        /// <param name="del">A delegate referencing a method, constructor or property.</param>
        /// <param name="expectedExceptionType">The <see cref="Type"/> of the exception expected to the be thrown.</param>
        /// <param name="errorMessageEnding">A message to include after any test failures.</param>
        /// <param name="args">The arguments to pass in to <paramref name="del"/>.</param>
        /// <typeparam name="TReturn">The type parameter assigned to the <see cref="IEnumerable{T}"/> or <see cref="IEnumerator{T}"/>.</typeparam>
        public static void ForDeferredMethod<TReturn> (Delegate del, Type expectedExceptionType, string errorMessageEnding, params object [] args)
        {
            var name = del.Method.Name;
            Type type = del.Method.ReturnType;

            if (typeof (IEnumerable).IsAssignableFrom (type) || typeof (IEnumerable<TReturn>).IsAssignableFrom (type))
            {
                var oldDel = del;
                del = (Func<Delegate, object [], bool>)DelegateForIEnumerable;
                args = new object [] { oldDel, args };
            }
            else if (typeof (IEnumerator).IsAssignableFrom (type) || typeof (IEnumerator<TReturn>).IsAssignableFrom (type))
            {
                var oldDel = del;
                del = (Func<Delegate, object [], bool>)DelegateForIEnumerator;
                args = new object [] { oldDel, args };
            }

            MoreAsserts.ExceptionThrown (del.DynamicInvoke, "Method " + name, expectedExceptionType, errorMessageEnding, args);
        }



        /// <summary>
        /// Tests that the instance indexing property throws the appropriate exception given the provided arguments.
        /// </summary>
        /// <param name="target">The instance that will be tested.</param>
        /// <param name="indexCount">A bad index value.</param>
        /// <typeparam name="TException">The expected exception type parameter.</typeparam>
        public static void ForInstanceIndexProperty<TException> (object target, int indexCount) where TException : Exception
        {
            ForInstanceIndexProperty<TException> (target, string.Empty, indexCount);
        }

        /// <summary>
        /// Tests that the instance indexing property throws the appropriate exception given the provided arguments.
        /// </summary>
        /// <param name="target">The instance that will be tested.</param>
        /// <param name="errorMessageEnding">A message to include after any test failures.</param>
        /// <param name="indexCount">A bad index value.</param>
        /// <typeparam name="TException">The expected exception type parameter.</typeparam>
        public static void ForInstanceIndexProperty<TException> (object target, string errorMessageEnding, int indexCount) where TException : Exception
        {
            ForInstanceIndexProperty (target, typeof (TException), errorMessageEnding, indexCount);
        }

        /// <summary>
        /// Tests that the instance indexing property throws the appropriate exception given the provided arguments.
        /// </summary>
        /// <param name="target">The instance that will be tested.</param>
        /// <param name="expectedExceptionType">The <see cref="Type"/> of the exception expected to the be thrown.</param>
        /// <param name="indexCount">A bad index value.</param>
        public static void ForInstanceIndexProperty (object target, Type expectedExceptionType, int indexCount)
        {
            ForInstanceIndexProperty (target, expectedExceptionType, string.Empty, indexCount);
        }

        /// <summary>
        /// Tests that the instance indexing property throws the appropriate exception given the provided arguments.
        /// </summary>
        /// <param name="target">The instance that will be tested.</param>
        /// <param name="expectedExceptionType">The <see cref="Type"/> of the exception expected to the be thrown.</param>
        /// <param name="errorMessageEnding">A message to include after any test failures.</param>
        /// <param name="indexCount">A bad index value.</param>
        public static void ForInstanceIndexProperty (object target, Type expectedExceptionType, string errorMessageEnding, int indexCount)
        {
            IndexGetterPropertyTestHelper (target.GetType (), target, _inst, expectedExceptionType, errorMessageEnding, indexCount);
        }



        /// <summary>
        /// Tests that the provided instance property throws the appropriate exception given the provided arguments.
        /// </summary>
        /// <param name="prop">The property to test.</param>
        /// <param name="target">The instance that will be tested.</param>
        /// <typeparam name="TException">The expected exception type parameter.</typeparam>
        public static void ForInstanceProperty<TException> (PropertyInfo prop, object target) where TException : Exception
        {
            ForInstanceProperty<TException> (prop, target, string.Empty);
        }

        /// <summary>
        /// Tests that the provided instance property throws the appropriate exception given the provided arguments.
        /// </summary>
        /// <param name="prop">The property to test.</param>
        /// <param name="target">The instance that will be tested.</param>
        /// <param name="errorMessageEnding">A message to include after any test failures.</param>
        /// <typeparam name="TException">The expected exception type parameter.</typeparam>
        public static void ForInstanceProperty<TException> (PropertyInfo prop, object target, string errorMessageEnding) where TException : Exception
        {
            ForInstanceProperty (prop, target, typeof (TException), errorMessageEnding);
        }

        /// <summary>
        /// Tests that the provided instance property throws the appropriate exception given the provided arguments.
        /// </summary>
        /// <param name="prop">The property to test.</param>
        /// <param name="target">The instance that will be tested.</param>
        /// <param name="expectedExceptionType">The <see cref="Type"/> of the exception expected to the be thrown.</param>
        public static void ForInstanceProperty (PropertyInfo prop, object target, Type expectedExceptionType)
        {
            ForInstanceProperty (prop, target, expectedExceptionType, string.Empty);
        }

        /// <summary>
        /// Tests that the provided instance property throws the appropriate exception given the provided arguments.
        /// </summary>
        /// <param name="prop">The property to test.</param>
        /// <param name="target">The instance that will be tested.</param>
        /// <param name="expectedExceptionType">The <see cref="Type"/> of the exception expected to the be thrown.</param>
        /// <param name="errorMessageEnding">A message to include after any test failures.</param>
        public static void ForInstanceProperty (PropertyInfo prop, object target, Type expectedExceptionType, string errorMessageEnding)
        {
            MoreAsserts.ObjectIsNull (prop, nameof (prop));

            GetterPropertyTestHelper (prop, target, prop.Name, expectedExceptionType, errorMessageEnding, 0);
        }



        /// <summary>
        /// Tests that the provided method throws the appropriate exception given the provided arguments. Testing deferred methods should use <see cref="ForDeferredMethod{T, TException} (Delegate, object[])"/>.
        /// </summary>
        /// <param name="del">A delegate referencing a method, constructor or property.</param>
        /// <param name="args">The arguments to pass in to <paramref name="del"/>.</param>
        /// <typeparam name="TException">The expected exception type parameter.</typeparam>
        public static void ForMethod<TException> (Delegate del, params object[] args) where TException : Exception
        {
            ForMethod<TException> (del, string.Empty, args);
        }

        /// <summary>
        /// Tests that the provided method throws the appropriate exception given the provided arguments. Testing deferred methods should use <see cref="ForDeferredMethod{T, TException} (Delegate, string, object[])"/>.
        /// </summary>
        /// <param name="del">A delegate referencing a method, constructor or property.</param>
        /// <param name="errorMessageEnding">A message to include after any test failures.</param>
        /// <param name="args">The arguments to pass in to <paramref name="del"/>.</param>
        /// <typeparam name="TException">The expected exception type parameter.</typeparam>
        public static void ForMethod<TException> (Delegate del, string errorMessageEnding, params object [] args) where TException : Exception
        {
            ForMethod (del, typeof (TException), errorMessageEnding, args);
        }

        /// <summary>
        /// Tests that the provided method throws the appropriate exception given the provided arguments. Testing deferred methods should use <see cref="ForDeferredMethod{T} (Delegate, Type, object[])"/>.
        /// </summary>
        /// <param name="del">A delegate referencing a method, constructor or property.</param>
        /// <param name="expectedExceptionType">The <see cref="Type"/> of the exception expected to the be thrown.</param>
        /// <param name="args">The arguments to pass in to <paramref name="del"/>.</param>
        public static void ForMethod (Delegate del, Type expectedExceptionType, params object[] args)
        {
            ForMethod (del, expectedExceptionType, string.Empty, args);
        }

        /// <summary>
        /// Tests that the provided method throws the appropriate exception given the provided arguments. Testing deferred methods should use <see cref="ForDeferredMethod{T} (Delegate, Type, string, object[])"/>.
        /// </summary>
        /// <param name="del">A delegate referencing a method, constructor or property.</param>
        /// <param name="expectedExceptionType">The <see cref="Type"/> of the exception expected to the be thrown.</param>
        /// <param name="errorMessageEnding">A message to include after any test failures.</param>
        /// <param name="args">The arguments to pass in to <paramref name="del"/>.</param>
        public static void ForMethod (Delegate del, Type expectedExceptionType, string errorMessageEnding, params object [] args)
        {
            MoreAsserts.ExceptionThrown (del.DynamicInvoke, "Method", expectedExceptionType, " This might be due to deferred execution. " + errorMessageEnding, args);
        }



        /// <summary>
        /// Tests that the static indexing property throws the appropriate exception given the provided arguments.
        /// </summary>
        /// <param name="indexCount">A bad index value.</param>
        /// <typeparam name="TDeclaring">The property declaring type parameter.</typeparam>
        /// <typeparam name="TException">The expected exception type parameter.</typeparam>
        public static void ForStaticIndexProperty<TDeclaring, TException> (int indexCount) where TException : Exception
        {
            ForStaticIndexProperty<TDeclaring, TException> (string.Empty, indexCount);
        }

        /// <summary>
        /// Tests that the static indexing property throws the appropriate exception given the provided arguments.
        /// </summary>
        /// <param name="errorMessageEnding">A message to include after any test failures.</param>
        /// <param name="indexCount">A bad index value.</param>
        /// <typeparam name="TDeclaring">The property declaring type parameter.</typeparam>
        /// <typeparam name="TException">The expected exception type parameter.</typeparam>
        public static void ForStaticIndexProperty<TDeclaring, TException> (string errorMessageEnding, int indexCount) where TException : Exception
        {
            ForStaticIndexProperty (typeof (TDeclaring), typeof (TException), errorMessageEnding, indexCount);
        }

        /// <summary>
        /// Tests that the static indexing property throws the appropriate exception given the provided arguments.
        /// </summary>
        /// <param name="declaringObjectType">The type declaring the indexing property.</param>
        /// <param name="indexCount">A bad index value.</param>
        /// <typeparam name="TException">The expected exception type parameter.</typeparam>
        public static void ForStaticIndexProperty<TException> (Type declaringObjectType, int indexCount) where TException : Exception
        {
            ForStaticIndexProperty<TException> (declaringObjectType, string.Empty, indexCount);
        }

        /// <summary>
        /// Tests that the static indexing property throws the appropriate exception given the provided arguments.
        /// </summary>
        /// <param name="declaringObjectType">The type declaring the indexing property.</param>
        /// <param name="errorMessageEnding">A message to include after any test failures.</param>
        /// <param name="indexCount">A bad index value.</param>
        /// <typeparam name="TException">The expected exception type parameter.</typeparam>
        public static void ForStaticIndexProperty<TException> (Type declaringObjectType, string errorMessageEnding, int indexCount) where TException : Exception
        {
            ForStaticIndexProperty (declaringObjectType, typeof (TException), errorMessageEnding, indexCount);
        }

        /// <summary>
        /// Tests that the static indexing property throws the appropriate exception given the provided arguments.
        /// </summary>
        /// <param name="declaringObjectType">The type declaring the indexing property.</param>
        /// <param name="expectedExceptionType">The <see cref="Type"/> of the exception expected to the be thrown.</param>
        /// <param name="indexCount">A bad index value.</param>
        public static void ForStaticIndexProperty (Type declaringObjectType, Type expectedExceptionType, int indexCount)
        {
            ForStaticIndexProperty (declaringObjectType, expectedExceptionType, string.Empty, indexCount);
        }

        /// <summary>
        /// Tests that the static indexing property throws the appropriate exception given the provided arguments.
        /// </summary>
        /// <param name="declaringObjectType">The type declaring the indexing property.</param>
        /// <param name="expectedExceptionType">The <see cref="Type"/> of the exception expected to the be thrown.</param>
        /// <param name="errorMessageEnding">A message to include after any test failures.</param>
        /// <param name="indexCount">A bad index value.</param>
        public static void ForStaticIndexProperty (Type declaringObjectType, Type expectedExceptionType, string errorMessageEnding, int indexCount)
        {
            IndexGetterPropertyTestHelper (declaringObjectType, null, _stat, expectedExceptionType, errorMessageEnding, indexCount);
        }



        /// <summary>
        /// Tests that the provided static property throws the appropriate exception given the provided arguments.
        /// </summary>
        /// <param name="prop">The property to test.</param>
        /// <typeparam name="TException">The expected exception type parameter.</typeparam>
        public static void ForStaticProperty<TException> (PropertyInfo prop) where TException : Exception
        {
            ForStaticProperty<TException> (prop, string.Empty);
        }

        /// <summary>
        /// Tests that the provided static property throws the appropriate exception given the provided arguments.
        /// </summary>
        /// <param name="prop">The property to test.</param>
        /// <param name="errorMessageEnding">A message to include after any test failures.</param>
        /// <typeparam name="TException">The expected exception type parameter.</typeparam>
        public static void ForStaticProperty<TException> (PropertyInfo prop, string errorMessageEnding) where TException : Exception
        {
            ForStaticProperty (prop, typeof (TException), errorMessageEnding);
        }

        /// <summary>
        /// Tests that the provided static property throws the appropriate exception given the provided arguments.
        /// </summary>
        /// <param name="prop">The property to test.</param>
        /// <param name="expectedException">The <see cref="Type"/> of the exception expected to the be thrown.</param>
        public static void ForStaticProperty (PropertyInfo prop, Type expectedException)
        {
            ForStaticProperty (prop, expectedException, string.Empty);
        }

        /// <summary>
        /// Tests that the provided static property throws the appropriate exception given the provided arguments.
        /// </summary>
        /// <param name="prop">The property to test.</param>
        /// <param name="expectedException">The <see cref="Type"/> of the exception expected to the be thrown.</param>
        /// <param name="errorMessageEnding">A message to include after any test failures.</param>
        public static void ForStaticProperty (PropertyInfo prop, Type expectedException, string errorMessageEnding)
        {
            MoreAsserts.ObjectIsNull (prop, nameof (prop));

            GetterPropertyTestHelper (prop, null, prop.Name, expectedException, errorMessageEnding, 0);
        }
    }
}
