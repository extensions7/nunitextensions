﻿/*
   Copyright 2022 OtherDelegates.cs

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace NUnitExtensions
{
    delegate void GenAction<TG> ();

    delegate void GenAction<TG, T>
        (T a);

    delegate void GenAction<TG, T1, T2>
        (T1 a, T2 b);

    delegate void GenAction<TG, T1, T2, T3>
        (T1 a, T2 b, T3 c);

    delegate void GenAction<TG, T1, T2, T3, T4>
        (T1 a, T2 b, T3 c, T4 d);

    delegate void GenAction<TG, T1, T2, T3, T4, T5>
        (T1 a, T2 b, T3 c, T4 d, T5 e);

    delegate void GenAction<TG, T1, T2, T3, T4, T5, T6>
        (T1 a, T2 b, T3 c, T4 d, T5 e, T6 f);

    delegate void GenAction<TG, T1, T2, T3, T4, T5, T6, T7>
        (T1 a, T2 b, T3 c, T4 d, T5 e, T6 f, T7 g);

    delegate void GenAction<TG, T1, T2, T3, T4, T5, T6, T7, T8>
        (T1 a, T2 b, T3 c, T4 d, T5 e, T6 f, T7 g, T8 h);

    delegate void GenAction<TG, T1, T2, T3, T4, T5, T6, T7, T8, T9>
        (T1 a, T2 b, T3 c, T4 d, T5 e, T6 f, T7 g, T8 h, T9 i);

    delegate void GenAction<TG, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>
        (T1 a, T2 b, T3 c, T4 d, T5 e, T6 f, T7 g, T8 h, T9 i, T10 j);

    delegate void GenAction<TG, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>
        (T1 a, T2 b, T3 c, T4 d, T5 e, T6 f, T7 g, T8 h, T9 i, T10 j, T11 k);

    delegate void GenAction<TG, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>
        (T1 a, T2 b, T3 c, T4 d, T5 e, T6 f, T7 g, T8 h, T9 i, T10 j, T11 k, T12 l);

    delegate void GenAction<TG, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>
        (T1 a, T2 b, T3 c, T4 d, T5 e, T6 f, T7 g, T8 h, T9 i, T10 j, T11 k, T12 l, T13 m);

    delegate void GenAction<TG, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>
        (T1 a, T2 b, T3 c, T4 d, T5 e, T6 f, T7 g, T8 h, T9 i, T10 j, T11 k, T12 l, T13 m, T14 n);

    delegate void GenAction<TG, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>
        (T1 a, T2 b, T3 c, T4 d, T5 e, T6 f, T7 g, T8 h, T9 i, T10 j, T11 k, T12 l, T13 m, T14 n, T15 o);

    delegate void GenAction<TG, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>
        (T1 a, T2 b, T3 c, T4 d, T5 e, T6 f, T7 g, T8 h, T9 i, T10 j, T11 k, T12 l, T13 m, T14 n, T15 o, T16 p);
}
