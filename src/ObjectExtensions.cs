﻿/*
   Copyright 2022 ObjectExtensions.cs

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace NUnitExtensions
{
    internal static class ObjectExtensions
    {
        /// <summary>
        /// Checks if an object is null.
        /// </summary>
        /// <returns><c>true</c>, if <paramref name="obj"/> is null, <c>false</c> otherwise.</returns>
        /// <param name="obj">The object to check if it is null.</param>
        internal static bool IsNull (this object? obj)
        {
            return ReferenceEquals (obj, null);
        }
    }
}
