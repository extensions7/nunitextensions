﻿/*
   Copyright 2022 DebugStringGenerator.cs

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace NUnitExtensions
{
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Defines the <see cref="DebugStringGenerator"/> class.
    /// </summary>
    public static class DebugStringGenerator
    {
        private const string format = "Expected: [{0}] Actual: [{1}]";

        public static string Format = format;

        private static string CTS<T> (this IReadOnlyList<T> list)
        {
            if (ReferenceEquals (list, null))
                return "null";

            var builder = new StringBuilder ();
            builder.Append (list [0].ToString ());
            for (var i = 1; i < list.Count; i++)
            {
                builder.Append (", " + list [i]);
            }

            return builder.ToString ();
        }

        /// <summary>
        /// Generates a standard formatted string using
        /// </summary>
        /// <param name="expected"></param>
        /// <param name="actual"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static string Generate<T> (IReadOnlyList<T> expected, IReadOnlyList<T> actual)
        {
            return string.Format (Format, expected.CTS (), actual.CTS ());
        }
    }
}
