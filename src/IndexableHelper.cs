﻿/*
   Copyright 2022 IndexableHelper.cs

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace NUnitExtensions
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    /// <summary>
    /// Defines the <see cref="IndexableHelper{T}"/> struct. This is meant to aid in indexing 
    /// </summary>
    /// <typeparam name="T">Any type</typeparam>
    public struct IndexableHelper<T> : IReadOnlyList<T>
    {
        private readonly IList<T>? _list;

        private readonly IReadOnlyList<T>? _rolist;

        private readonly bool _useList;

        /// <summary>
        /// Initializes a new instance of the <see cref="IndexableHelper{T}"/> generic struct.
        /// </summary>
        /// <param name="list">An instance of the <see cref="List{T}"/> class.</param>
        internal IndexableHelper (List<T> list) :
            this (list as IList<T>)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="IndexableHelper{T}"/> generic struct.
        /// </summary>
        /// <param name="array">An instance of the <see cref="Array"/> class.</param>
        public IndexableHelper (T [] array) :
            this (array as IReadOnlyList<T>)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="IndexableHelper{T}"/> generic struct.
        /// </summary>
        /// <param name="list">An object that implements the <see cref="IList{T}"/> interface.</param>
        public IndexableHelper (IList<T> list)
        {
            if (list.IsNull ())
                throw new ArgumentNullException (nameof (list));

            this._list = list;
            this._rolist = null;
            this._useList = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IndexableHelper{T}"/> generic struct.
        /// </summary>
        /// <param name="rolist">An object that implements the <see cref="IReadOnlyList{T}"/> interface.</param>
        public IndexableHelper (IReadOnlyList<T> rolist)
        {
            if (rolist.IsNull ())
                throw new ArgumentNullException (nameof (rolist));

            this._list = null;
            this._rolist = rolist;
            this._useList = false;
        }

        public T this [int index] { get => this._useList ? this._list [index] : this._rolist [index]; }

        public int Count { get => this._useList ? this._list.Count : this._rolist.Count; }

        public IEnumerator<T> GetEnumerator () => this._useList ? this._list.GetEnumerator () : this._rolist.GetEnumerator ();

        IEnumerator IEnumerable.GetEnumerator () => this.GetEnumerator ();
    }
}
