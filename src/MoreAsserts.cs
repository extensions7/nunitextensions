﻿/*
   Copyright 2022 MoreAsserts.cs

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace NUnitExtensions
{
    using System;

    using NUnit.Framework;

    public static class MoreAsserts
    {
        /// <summary>
        /// Asserts the object is null.
        /// </summary>
        /// <param name="param">The object to check.</param>
        /// <param name="nameOfParam">The name of <paramref name="param"/>.</param>
        internal static void ObjectIsNull (object param, string nameOfParam)
        {
            Assert.IsFalse (param.IsNull (), nameOfParam + " is null.");
        }

        /// <summary>
        /// Asserts that the provided delegate throws the provided exception type.
        /// </summary>
        /// <param name="delegateDynamicInvoke">This should be <see cref="Delegate.DynamicInvoke(object[])"/></param>
        /// <param name="delName">The name of <paramref name="delegateDynamicInvoke"/> to place in test failure messages.</param>
        /// <param name="errorMessageEnding">A message to include after any test failures.</param>
        /// <param name="args">The arguments needed to invoke the <paramref name="delegateDynamicInvoke"/>.</param>
        /// <typeparam name="TException">The exception type expected to the be thrown.</typeparam>
        public static void ExceptionThrown<TException> (Func<object [], object> delegateDynamicInvoke, string delName, string errorMessageEnding, object [] args)
            where TException : Exception
        {
            ExceptionThrown (delegateDynamicInvoke, delName, typeof (TException), errorMessageEnding, args);
        }

        /// <summary>
        /// Asserts that the provided delegate throws the provided exception type.
        /// </summary>
        /// <param name="delegateDynamicInvoke">This should be <see cref="Delegate.DynamicInvoke(object[])"/></param>
        /// <param name="delName">The name of <paramref name="delegateDynamicInvoke"/> to place in test failure messages.</param>
        /// <param name="args">The arguments needed to invoke the <paramref name="delegateDynamicInvoke"/>.</param>
        /// <typeparam name="TException">The exception type expected to the be thrown.</typeparam>
        public static void ExceptionThrown<TException> (Func<object [], object> delegateDynamicInvoke, string delName, object [] args)
            where TException : Exception
        {
            ExceptionThrown<TException> (delegateDynamicInvoke, delName, string.Empty, args);
        }

        /// <summary>
        /// Asserts that the provided delegate throws the provided exception type.
        /// </summary>
        /// <param name="delegateDynamicInvoke">This should be <see cref="Delegate.DynamicInvoke(object[])"/></param>
        /// <param name="delName">The name of <paramref name="delegateDynamicInvoke"/> to place in test failure messages.</param>
        /// <param name="expectedExceptionType">The <see cref="Type"/> of the exception expected to the be thrown.</param>
        /// <param name="args">The arguments needed to invoke the <paramref name="delegateDynamicInvoke"/>.</param>
        public static void ExceptionThrown (Func<object [], object> delegateDynamicInvoke, string delName, Type expectedExceptionType, object [] args)
        {
            ExceptionThrown (delegateDynamicInvoke, delName, expectedExceptionType, string.Empty, args);
        }

        /// <summary>
        /// Asserts that the provided delegate throws the provided exception type.
        /// </summary>
        /// <param name="delegateDynamicInvoke">This should be <see cref="Delegate.DynamicInvoke(object[])"/></param>
        /// <param name="delName">The name of <paramref name="delegateDynamicInvoke"/> to place in test failure messages.</param>
        /// <param name="expectedExceptionType">The <see cref="Type"/> of the exception expected to the be thrown.</param>
        /// <param name="errorMessageEnding">A message to include after any test failures.</param>
        /// <param name="args">The arguments needed to invoke the <paramref name="delegateDynamicInvoke"/>.</param>
        public static void ExceptionThrown (Func<object [], object> delegateDynamicInvoke, string delName, Type expectedExceptionType, string errorMessageEnding, object [] args)
        {
            ObjectIsNull (delegateDynamicInvoke, delName);
            ObjectIsNull (expectedExceptionType, nameof (expectedExceptionType));
            if (errorMessageEnding.IsNull ())
                errorMessageEnding = string.Empty;

            var threw = false;

            try
            {
                var foo = delegateDynamicInvoke (args);
            }
            catch (Exception e)
            {
                Type actual;
                string message;

                ExceptionHelpers.GetInnerExceptionData (e, out actual, out message);

                var result = expectedExceptionType.Equals (actual);
                Assert.IsTrue (result, string.Format ("{0} did not throw the correct exception type.{1} Expected:{2} Actual:{3} {4}", delName, errorMessageEnding, expectedExceptionType, actual, message));
                threw = true;
            }

            // This can be reached if the delegate is for a deferred execution method.
            Assert.IsTrue (threw, delName + " did not throw an exception. " + errorMessageEnding);
        }
    }
}
