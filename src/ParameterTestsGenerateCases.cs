﻿/*
   Copyright 2022 ParameterTestsGenerateCases.cs

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

namespace NUnitExtensions
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    public static partial class ParameterTests
    {
        /// <summary>
        /// Gets an object array that contains a single null.
        /// </summary>
        public static object? [] OneNullArg { get => new object? [] { null }; }

        private static void ParameterChecks (int index, string indexName, ICollection args, string argsName)
        {
            if (ReferenceEquals (args, null))
                throw new ArgumentNullException (argsName);

            if (index < 0)
                throw new ArgumentException (indexName + "must be greater than -1.");

            if (index > args.Count)
                throw new ArgumentException (indexName + "must be less than or equal to " + argsName + ".Length.");
        }

        /// <summary>
        /// Generates a set of test cases with the exception type placed as the last index.
        /// </summary>
        /// <returns>A collection of test cases with the expected exception type is at the last index.</returns>
        /// <param name="args">A collection of <see cref="GoodAndBadParameterValues"/> representing a good and a bad value along with the expected exception type for the bad value.</param>
        public static IEnumerable<object []> GenerateCases (params GoodAndBadParameterValues [] args)
        {
            return GenerateCases (args.Length, args);
        }

        /// <summary>
        /// Generates a set of test cases with the exception type placed at the provided index.
        /// </summary>
        /// <returns>A collection of test cases with the expected exception type is at the provided index.</returns>
        /// <param name="exceptionTypeIndex">The index in which the expected exception type should be inserted.</param>
        /// <param name="args">A collection of <see cref="GoodAndBadParameterValues"/> representing a good and a bad value along with the expected exception type for the bad value.</param>
        public static IEnumerable<object []> GenerateCases (int exceptionTypeIndex, params GoodAndBadParameterValues [] args)
        {
            ParameterChecks (exceptionTypeIndex, nameof (exceptionTypeIndex), args, nameof (args));

            for (var badIndex = 0; badIndex < args.Length; badIndex++)
            {
                if (args [badIndex].IsUsable)
                {
                    var result = new object [args.Length + 1];
                    // The following code copies all the good values from args to result, ...
                    //      then swaps out the bad value at the appropriate index, ...
                    //      then moves all values over 1 index to allow for the exception type to be inserted, ...
                    //      and finally inserts the exception type.
                    // This may not be the most efficient means of doing this, but it's effective.

                    for (var i = 0; i < args.Length; i++)
                        result [i] = args [i].Good;

                    result [badIndex] = args [badIndex].Bad;

                    for (var i = result.Length - 1; i > exceptionTypeIndex; i--)
                        result [i] = result [i - 1];

                    result [exceptionTypeIndex] = args [badIndex].ExceptionType;

                    yield return result;
                }
            }
        }

        /// <summary>
        /// Generates a set of test cases with the exception type for <see cref="ArgumentNullException"/> placed as the last index.
        /// </summary>
        /// <returns>A collection of test cases with the expected exception type is at the last index.</returns>
        /// <param name="args">A collection of objects representing the good value where null is the bad value.</param>
        public static IEnumerable<object []> GenerateCases (params object [] args)
        {
            return GenerateCases (typeof (ArgumentNullException), args);
        }

        /// <summary>
        /// Generates a set of test cases with the exception type placed as the last index.
        /// </summary>
        /// <returns>A collection of test cases with the expected exception type is at the last index.</returns>
        /// <param name="expectedExceptionType">The expected exception type.</param>
        /// <param name="args">A collection of objects representing the good value where null is the bad value.</param>
        public static IEnumerable<object []> GenerateCases (Type expectedExceptionType, params object [] args)
        {
            return GenerateCases (args.Length, expectedExceptionType, args);
        }

        /// <summary>
        /// Generates a set of test cases with the exception type placed as the last index.
        /// </summary>
        /// <returns>A collection of test cases with the expected exception type is at the last index.</returns>
        /// <param name="expectedExceptionTypes">The expected exception type. This must be the same length as <paramref name="args"/>. If an element is null, then that index will be skipped.</param>
        /// <param name="args">A collection of objects representing the good value where null is the bad value.</param>
        public static IEnumerable<object []> GenerateCases (Type [] expectedExceptionTypes, params object [] args)
        {
            return GenerateCases (args.Length, expectedExceptionTypes, args);
        }

        /// <summary>
        /// Generates a set of test cases with the exception type placed at the provided index.
        /// </summary>
        /// <returns>A collection of test cases with the expected exception type is at the provided index.</returns>
        /// <param name="exceptionTypeIndex">The index in which the expected exception type should be inserted.</param>
        /// <param name="expectedExceptionType">The expected exception type.</param>
        /// <param name="args">A collection of objects representing the good value where null is the bad value.</param>
        public static IEnumerable<object []> GenerateCases (int exceptionTypeIndex, Type expectedExceptionType, params object [] args)
        {
            var types = args.Select (a => expectedExceptionType).ToArray ();
            return GenerateCases (exceptionTypeIndex, types, args);
        }

        /// <summary>
        /// Generates a set of test cases with the exception type placed at the provided index.
        /// </summary>
        /// <returns>A collection of test cases with the expected exception type is at the provided index.</returns>
        /// <param name="exceptionTypeIndex">The index in which the expected exception type should be inserted.</param>
        /// <param name="expectedExceptionTypes">The expected exception type. This must be the same length as <paramref name="args"/>. If an element is null, then that index will be skipped.</param>
        /// <param name="args">A collection of objects representing the good value where null is the bad value.</param>
        public static IEnumerable<object []> GenerateCases (int exceptionTypeIndex, Type [] expectedExceptionTypes, params object [] args)
        {
            // check some of the parameters
            ParameterChecks (exceptionTypeIndex, nameof (exceptionTypeIndex), args, nameof (args));
            if (ReferenceEquals (expectedExceptionTypes, null))
                throw new ArgumentNullException (nameof (expectedExceptionTypes));

            for (var i = 0; i < args.Length; i++)
                if (args [i] is ValueType)
                    throw new ArgumentException ("Cannot use a value type in " + nameof (args));

            if (args.Length != expectedExceptionTypes.Length)
                throw new ArgumentException (nameof (args) + " and " + nameof (expectedExceptionTypes) + " must be the same length.");

            for (var badIndex = 0; badIndex < args.Length; badIndex++)
            {
                if (!ReferenceEquals (expectedExceptionTypes [badIndex], null))
                {
                    var result = new object [args.Length + 1];
                    for (var i = 0; i < args.Length; i++)
                        result [i] = args [i];

                    result [badIndex] = null;

                    for (var i = result.Length - 1; i > exceptionTypeIndex; i--)
                        result [i] = result [i - 1];

                    result [exceptionTypeIndex] = expectedExceptionTypes [badIndex];

                    yield return result;
                }
            }
        }

        private static ParameterInfo [] GetMethodSignature (this MethodInfo testMethod) => testMethod.GetParameters ();

        private static bool IsExceptionTypeGenericParameter (MethodInfo method)
        {
            var result = false;
            if (method.IsGenericMethod)
            {
                var genericParams = method.GetGenericArguments ();
                var exType = typeof (Exception);
                result = genericParams.Any (p => p == exType || p.IsSubclassOf (exType));
            }

            return result;
        }

        private static IEnumerable<object []> GenerateTestCasesForHelper (MethodInfo testMethod, ParameterInfo [] signature, bool exceptionIsTypeParameter, int expectedExceptionIndex, GoodAndBadParameterValues [] args)
        {
            if (ReferenceEquals (args, null))
                throw new ArgumentNullException (nameof (args));

            if (!exceptionIsTypeParameter && (expectedExceptionIndex < 0 || expectedExceptionIndex >= signature.Length))
                throw new ArgumentException ("Provided exception type index is invalid for a non-generic method.");

            // check that the order of args matches the method signature close enough.
            //      factor in that the expected exception index
            // Make a revised signature that excludes the expected exception type parameter if necessary.

            Type [] revisedSig;
            if (exceptionIsTypeParameter)
                revisedSig = signature.Select (i => i.ParameterType).ToArray ();
            else
            {
                revisedSig = new Type [signature.Length];
                for (var i = 0; i < signature.Length; i++)
                    revisedSig [i] = signature [i].ParameterType;

                var max = revisedSig.Length - 1;
                for (var i = expectedExceptionIndex; i < max; i++)
                    revisedSig [i] = revisedSig [i + 1];
            }

            // Now check that the args match the revised signature.

            for (var i = 0; i < args.Length; i++)
                if (args [i].ParameterType != revisedSig [i])
                    throw new ArgumentException ("Provided arguments does not match the delegate's signature closely enough. This does take the expected exception into account.");

            // finally construct the output arrays.

            for (var badIndex = 0; badIndex < args.Length; badIndex++)
            {
                if (args [badIndex].IsUsable)
                {
                    var result = new object [signature.Length];
                    for (var i = 0; i < args.Length; i++)
                        result [i] = args [i].Good;

                    result [badIndex] = args [badIndex].Bad;

                    if (!exceptionIsTypeParameter)
                    {
                        for (var i = args.Length; i > expectedExceptionIndex; i--)
                            result [i] = result [i - 1];

                        result [expectedExceptionIndex] = args [badIndex].ExceptionType;
                    }

                    yield return result;
                }
            }
        }

        public static IEnumerable<object []> GenerateCasesFor (Delegate testMethod, params GoodAndBadParameterValues [] args)
        {
            if (ReferenceEquals (testMethod, null))
                throw new ArgumentNullException (nameof (testMethod));

            var method = testMethod.Method;
            var signature = method.GetMethodSignature ();
            var exceptionIsTypeParameter = IsExceptionTypeGenericParameter (method);
            var index = -1;
            if (exceptionIsTypeParameter)
            {
                var typeType = typeof (Type);
                for (var i = 0; i < signature.Length; i++)
                {
                    if (signature [i].ParameterType == typeType)
                    {
                        if (index > -1)
                            throw new ArgumentException
                                ("Method signature has multiple parameters of the Type type." +
                                 " Unable to determine which parameter should be used for passing in the expected exception type." +
                                 " Use the overload that accepts the expected exception index or make the test method generic" +
                                 " where the type parameter is constrained to exceptions.");
                    }
                    else
                        index = i;
                }
            }

            return GenerateTestCasesForHelper (method, signature, exceptionIsTypeParameter, index, args);
        }

        public static IEnumerable<object []> GenerateCasesFor (Delegate testMethod, int expectedExceptionIndex, params GoodAndBadParameterValues [] args)
        {
            if (ReferenceEquals (testMethod, null))
                throw new ArgumentNullException (nameof (testMethod));

            var method = testMethod.Method;
            var signature = method.GetMethodSignature ();

            return GenerateTestCasesForHelper (method, signature, IsExceptionTypeGenericParameter (method), expectedExceptionIndex, args);
        }
    }
}
